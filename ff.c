#include "ff.h"

#ifdef NON_MACRO_RANDOM

unsigned int RANDOM() {
    unsigned int rr;
    rand_s(&rr);
    return rr;
}

#endif
